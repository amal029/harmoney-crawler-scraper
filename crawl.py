#!/usr/bin/env python3

import argparse
from selenium import webdriver
import csv
import time
import datetime


def login(driver=None, username='', password=''):
    u_element = driver.find_element_by_xpath('//input[@id="account_email"]')
    p_element = driver.find_element_by_xpath('//input[@id="account_password"]')
    s_button = driver.find_element_by_xpath('//input[@type="submit"]')
    u_element.send_keys(username)
    p_element.send_keys(password)
    s_button.click()


def ValConvert(val):
    if type(val).__name__ == 'unicode':
        return val.encode('utf8')
    elif type(val).__name__ == 'str':
        return val
    else:
        return str(val)


def getInfo(url='', driver=None, ofile='output.csv'):
    try:
        driver.get(url)
        time.sleep(5)
        fieldnames = ['LOAN-ID', 'GRADE AND RATE', 'TERM AND AMOUNT',
                      'FUNDED', 'REMAINING']
        for li in driver.find_elements_by_xpath(
                '//td[contains(@class,"loan-id")]'):
            li.click()
            time.sleep(5)
        count = 0
        rows = []
        for i in driver.find_elements_by_xpath(
                '//tbody[contains(@class, "ng-scope")]'):
            j = [j.rstrip() for j in ValConvert(i.text).split('\n')]
            if count % 2 == 0:    # This is the top level borrower data
                row = dict()
                for k, v in zip(fieldnames[0:5], j):
                    row.update({k: v.strip()})
            else:               # These are the borrower details
                cs = [c for x, c in zip(j, range(len(j)))
                      if x.endswith(':') or x.endswith(':**')]
                cv = [x+1 for x in cs]
                keys = [j[t].split(':')[0].strip() for t in cs]
                vals = [j[t] for t in cv]
                for k, v in zip(keys, vals):
                    row.update({k: v.strip()})
                ak = [k for k in keys if k not in fieldnames]
                fieldnames += ak
            rows += [row]
            count = count + 1

        # gr = list(set([r['GRADE AND RATE'].split()[0] for r in rows]))
        # If rows[1] is A then send email
        # print data to CSV file
        with open(ofile, 'w') as f:
            fieldnames.insert(0, 'DT')
            dt = datetime.datetime.now().strftime("%d/%m/%Y_%H/%M")
            writer = csv.DictWriter(f, fieldnames)
            writer.writeheader()
            for c, r in zip(range(len(rows)), rows):
                if c % 2 == 0:
                    r.update({'DT': dt})
                    writer.writerow(r)
    finally:
        if driver is not None:
            driver.quit()


def main(username='', password='', fn=''):
    # Top url
    urls = ['https://app.harmoney.com/accounts/sign_in',
            'https://www.harmoney.co.nz/lender/portal/invest/marketplace/browse']
    driver = webdriver.Chrome()  # The web driver
    driver.get(urls[0])  # Get the login webpage
    # The state is now different and logged in
    login(driver, username, password)
    # Get the information from the table
    getInfo(urls[1], driver, fn)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("username")
    parser.add_argument("password")
    parser.add_argument("output_file_name")
    args = parser.parse_args()
    main(args.username, args.password, args.output_file_name)
