#!/usr/bin/env python

from __future__ import division, absolute_import
from __future__ import print_function

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn import cross_validation as CV
from sklearn.preprocessing import LabelEncoder
# from astropy.table import Table
from cvxopt import matrix, solvers, blas
# from sklearn.cross_validation import cross_val_score
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score
from sklearn.metrics import mean_squared_error
from scipy.spatial.distance import cdist


def markowitz(test, axis, tech):
    # Now do Markowitz portfolio optimization

    N = 100
    # Targets rates
    mus = [10**(5.0 * t/N - 1.0) for t in range(N)]

    # Do optimization
    Sigma = matrix(np.diag(test['Var']))

    solvers.options['show_progress'] = False
    pbar = matrix(test['RR'], (len(test), 1), tc='d')
    portfolios = [solvers.qp(P=Sigma*mu, q=-pbar,
                             G=-matrix(np.eye(len(test))),
                             h=matrix(0.0, (len(test), 1)),
                             A=matrix(1.0, (1, len(test))),
                             b=matrix(1.0))['x'] for mu in mus]
    returns = [blas.dot(pbar, x) for x in portfolios]
    risks = [np.sqrt(blas.dot(x, Sigma*x)) for x in portfolios]
    # This is my sharpe ratio.
    # ANZ Bank Deposit (risk free investment = 0.75% return)
    sharpe_ratio = [(re - (0.75/100))/ri for re, ri in zip(returns, risks)]
    print ('Max sharpe ratio:', max(sharpe_ratio)*100, '%')
    # R_a = np.ndarray.tolist(R_a)
    axis.plot(risks, returns, 'o')
    axis.set_xlabel('Risk')
    axis.set_ylabel('Annualized Return Rate')
    axis.set_title(('Annualized Return vs Risk for ' + tech))


def kernel(x):
    return 1/(2*np.pi)**0.5*np.exp(-0.5*x**2)


def kernel_method(training_set, test_set, clf, R_train):
    l_1, l_2 = len(training_set), len(test_set)
    Y_train_l = clf.predict_proba(training_set)
    preds_l = clf.predict_proba(test_set)
    dmatrix_l = cdist(preds_l, Y_train_l, 'euclidean')
    sigma_l = Y_train_l.std()
    hl_0 = (4/3/l_1)**(1/5)*sigma_l
    h_l = [(0.25+i*0.1)*hl_0 for i in xrange(26)]
    CV_l = [np.nan]*26
    ydmat = cdist(Y_train_l, Y_train_l, 'euclidean')
    for i in xrange(0, 26):
        part_jl = 0
        for j in xrange(l_1):
            part_kl1 = 0
            part_kl2 = 0
            for k in xrange(l_1):
                if k != j:
                    part_kl1 += kernel(ydmat[j][k]/h_l[i])*R_train[k]
                    part_kl2 += kernel(ydmat[j][k]/h_l[i])
            part_jl += (part_kl1/part_kl2 - R_train[j])**2
        CV_l[i] = part_jl/l_1

    weight_l = np.array([np.nan]*l_1*l_2).reshape((l_2, l_1))
    part_k = [0]*l_2
    for i in xrange(l_2):
        for k in xrange(l_1):
            part_k[i] += kernel(dmatrix_l[i][k]/h_l[24])
    for i in xrange(l_2):
        for j in xrange(l_1):
            weight_l[i][j] = kernel(dmatrix_l[i][j]/h_l[24])/part_k[i]

    R_pred_l = [np.sum(np.multiply(R_train, np.array(w))) for w in weight_l]
    V_pred_l = [0]*l_2
    for i in xrange(l_2):
        for j in xrange(l_1):
            V_pred_l[i] += weight_l[i][j]*(R_train[j] - R_pred_l[i])**2

    return pd.DataFrame({'RR': R_pred_l, 'Var': V_pred_l})


def main():
    data = pd.read_csv('./prosperLoanData1.csv')
    data = data.sample(3000)      # Remove this for doing all data!
    data = data[['ListingKey', 'LoanStatus',
                 'BorrowerAPR', 'DebtToIncomeRatio', 'IsBorrowerHomeowner',
                 'InquiriesLast6Months', 'LoanOriginalAmount',
                 'CreditScoreRangeLower', 'EstimatedLoss',
                 'LP_CustomerPayments', 'LP_ServiceFees', 'Years', 'Term']]

    data = data[(data['LoanStatus'] == 'Completed')
                | (data['LoanStatus'] == 'Defaulted')]

    data = data[pd.notnull(data['DebtToIncomeRatio'])]
    data = data[pd.notnull(data['EstimatedLoss'])]

    data['LoanStatus'][data['LoanStatus'] == 'Defaulted'] = 0
    data['LoanStatus'][(data['Term'] > (data['Years'] * 12)) &
                       (data['LoanStatus'] == 'Completed')] = 2
    data['LoanStatus'][data['LoanStatus'] == 'Completed'] = 1

    # Calculate the annualized return rate
    data['Return_rate'] = (data['LP_CustomerPayments'] -
                           data['LP_ServiceFees']) / data['LoanOriginalAmount']

    data['Return_rate'] = data['Return_rate'] ** (1/data['Years']) - 1

    # Use label encoder
    leisbHO = LabelEncoder()
    data['IsBorrowerHomeowner'] = leisbHO.fit_transform(
        data['IsBorrowerHomeowner'])

    train_data, test_data = CV.train_test_split(data, test_size=0.3)

    exog = train_data[['DebtToIncomeRatio',
                       'IsBorrowerHomeowner',
                       'InquiriesLast6Months',
                       'LoanOriginalAmount',
                       'CreditScoreRangeLower']]

    endog = train_data['LoanStatus'].astype(float)

    # RandomForestClassifier
    rf = RandomForestClassifier()  # Just use default values
    # Now fit the values
    rf = rf.fit(exog, endog)

    # Now do some cross validation
    scores = CV.cross_val_score(rf, exog, endog,
                                scoring='accuracy', cv=10)

    # 95% confidence interval
    print ('95% confidence interval for accuracy_score:')
    print ('[', (scores.mean() - 2*(scores.std() / np.sqrt(10))), ',')
    print (scores.mean() + 2*(scores.std() / np.sqrt(10)), ']', '\n')

    prepayment, full, default = [], [], []
    # Now get the mean return and risk for each class
    for rr, c in zip(train_data['Return_rate'], rf.predict(exog)):
        if c == 2:
            prepayment += [rr]
        elif c == 1:
            full += [rr]
        else:
            default += [rr]

    prepayment = pd.DataFrame(data=prepayment, columns=['Prepayment'])
    full = pd.DataFrame(data=full, columns=['Full'])
    default = pd.DataFrame(data=default, columns=['Default'])

    print ('ARR Prepay:', prepayment.mean()[0], prepayment.std()[0], '\n')
    print ('ARR Full:', full.mean()[0], full.std()[0], '\n')
    print ('ARR Default:', default.mean()[0], default.std()[0], '\n')

    # Now do for the test data
    test_exog = test_data[['DebtToIncomeRatio',
                           'IsBorrowerHomeowner',
                           'InquiriesLast6Months',
                           'LoanOriginalAmount',
                           'CreditScoreRangeLower']]
    test_predicted_values = rf.predict(test_exog)
    actual_values = test_data['LoanStatus'].astype(float)

    print ('Test prediction accuracy:', accuracy_score(
        test_predicted_values, actual_values))

    test_rr = pd.DataFrame(np.zeros((len(actual_values), 2)),
                           columns=['RR', 'Var'])

    for i, t in zip(xrange(len(actual_values)), test_predicted_values):
        if t == 2:
            test_rr['RR'][i] = prepayment.mean()[0]
            test_rr['Var'][i] = np.square(prepayment.std()[0])
        elif t == 1:
            test_rr['RR'][i] = full.mean()[0]
            test_rr['Var'][i] = np.square(full.std()[0])
        else:
            test_rr['RR'][i] = default.mean()[0]
            test_rr['Var'][i] = np.square(default.std()[0])

    print ('RF mse:', mean_squared_error(
        test_data['Return_rate'], test_rr['RR']))

    # print test_rr.describe()

    # Let us now do kernel method
    kernel_rr = kernel_method(exog, test_exog, rf,
                              train_data['Return_rate'].values)
    print ('kernel mse:', mean_squared_error(
        test_data['Return_rate'], kernel_rr['RR']))

    # print kernel_rr.describe()

    # The markowitz portfolio optimization for both
    fig, ax = plt.subplots(1, 2)
    markowitz(test_rr, ax[0], 'RF')
    markowitz(kernel_rr, ax[1], 'Kernel')
    plt.show()


if __name__ == '__main__':
    main()
