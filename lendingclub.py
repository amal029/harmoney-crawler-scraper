# -*- coding: utf-8 -*-

from __future__ import division, absolute_import
from __future__ import print_function

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn import cross_validation as CV
# from sklearn.preprocessing import LabelEncoder
from cvxopt import matrix, solvers, blas
# from sklearn.cross_validation import cross_val_score
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score
from sklearn.metrics import mean_squared_error
from scipy.spatial.distance import cdist
from sklearn.linear_model import LogisticRegression
from astropy.table import Table


def markowitz(test, title, axis):
    # Now do Markowitz portfolio optimization

    N = 100
    # Targets rates
    mus = [10**(5.0 * t/N - 1.0) for t in range(N)]

    # Do optimization
    Sigma = matrix(np.diag(test['Var']))

    solvers.options['show_progress'] = False
    pbar = matrix(test['RR'], (len(test), 1), tc='d')
    portfolios = [solvers.qp(P=Sigma*mu, q=-pbar,
                             G=-matrix(np.eye(len(test))),
                             h=matrix(0.0, (len(test), 1)),
                             A=matrix(1.0, (1, len(test))),
                             b=matrix(1.0))['x'] for mu in mus]
    returns = [blas.dot(pbar, x) for x in portfolios]
    risks = [np.sqrt(blas.dot(x, Sigma*x)) for x in portfolios]
    # This is my sharpe ratio.
    # ANZ Bank Deposit (risk free investment = 0.75% return)
    # sharpe_ratio = [(re - (0.75/100))/ri for re, ri in zip(returns, risks)]
    # print 'Max sharpe ratio:', max(sharpe_ratio)*100, '%'
    # R_a = np.ndarray.tolist(R_a)
    axis.plot(risks, returns, 'o')
    # axis.set_xlabel('Risk')
    # axis.set_ylabel('Annualized Return Rate')
    axis.set_title(title)


def kernel(x):
    return 1/(2*np.pi)**0.5*np.exp(-0.5*x**2)


def kernel_method(training_set, test_set, clf, R_train):
    l_1, l_2 = len(training_set), len(test_set)
    Y_train_l = clf.predict_proba(training_set)
    preds_l = clf.predict_proba(test_set)
    dmatrix_l = cdist(preds_l, Y_train_l, 'euclidean')
    sigma_l = Y_train_l.std()
    hl_0 = (4/3/l_1)**(1/5)*sigma_l
    h_l = [(0.25+i*0.1)*hl_0 for i in range(26)]
    CV_l = [np.nan]*26
    ydmat = cdist(Y_train_l, Y_train_l, 'euclidean')
    for i in range(0, 26):
        part_jl = 0
        for j in range(l_1):
            part_kl1 = 0
            part_kl2 = 0
            for k in range(l_1):
                if k != j:
                    part_kl1 += kernel(ydmat[j][k]/h_l[i])*R_train[k]
                    part_kl2 += kernel(ydmat[j][k]/h_l[i])
            part_jl += (part_kl1/part_kl2 - R_train[j])**2
        CV_l[i] = part_jl/l_1
    # Let determine which bandwidth we should use. Find out the index of
    # the minimum of CV_l, and the corresponding h_l[index] is the best
    # bandwidth we should choose
    h = h_l[CV_l.index(min(CV_l))]
    weight_l = [[np.nan for x in range(l_1)] for y in range(l_2)]
    part_k = [0]*l_2
    for i in range(l_2):
        for k in range(l_1):
            part_k[i] += kernel(dmatrix_l[i][k]/h)
    for i in range(l_2):
        for j in range(l_1):
            weight_l[i][j] = kernel(dmatrix_l[i][j]/h)/part_k[i]

    # print [all(i == weight_l[j][0]
    #            for i in weight_l[j]) for j in xrange(l_2)]
    R_pred_l = [np.sum(np.multiply(R_train, np.array(w))) for w in weight_l]
    V_pred_l = [0]*l_2
    for i in range(l_2):
        for j in range(l_1):
            V_pred_l[i] += weight_l[i][j]*(R_train[j] - R_pred_l[i])**2

    return pd.DataFrame({'RR': R_pred_l, 'Var': V_pred_l})


def main():
    data = pd.read_csv('./LoanStats3a1.csv', encoding="ISO-8859-1")
    data = data[['id', 'loan_status', 'dti',
                 'home_ownership', 'inq_last_6mths',
                 'loan_amnt', 'total_pymnt',
                 'Years', 'Term']]

    data = data[(data['loan_status'] == 'Fully Paid') |
                (data['loan_status'] == 'Charged Off')]

    data = data[pd.notnull(data['dti'])]
    data = data[pd.notnull(data['inq_last_6mths'])]

    # Calculate the annualized return rate
    data['Return_rate'] = data['total_pymnt'] / data['loan_amnt']

    data['Return_rate'] = data['Return_rate'] ** (1/data['Years']) - 1

    # Change the home_ownership to 0,1
    data['home_ownership'][data['home_ownership'] != 'OWN'] = 0
    data['home_ownership'][data['home_ownership'] == 'OWN'] = 1

    data = data.sample(3000)      # Remove this for doing all data!

    train_data, test_data = CV.train_test_split(data, test_size=0.15)

    # data for original 2-label classification with kernel method
    train_data1 = train_data.copy()
    test_data1 = test_data.copy()
    train_data1['loan_status'][train_data1['loan_status'] == 'Charged Off'] = 0
    train_data1['loan_status'][train_data1['loan_status'] == 'Fully Paid'] = 1
    test_data1['loan_status'][test_data1['loan_status'] == 'Charged Off'] = 0
    test_data1['loan_status'][test_data1['loan_status'] == 'Fully Paid'] = 1

    exog1 = train_data1[['dti',
                         'home_ownership',
                         'inq_last_6mths',
                         'loan_amnt']]

    # How do you know that this is encoded into 2 labels correctly?
    endog1 = train_data1['loan_status'].astype(float)

    # data for 3-label classification
    train_data['loan_status'][train_data['loan_status'] == 'Charged Off'] = 0
    train_data['loan_status'][(train_data['Term'] >
                               (train_data['Years'] * 12)) &
                              (train_data['loan_status'] == 'Fully Paid')] = 2
    train_data['loan_status'][train_data['loan_status'] == 'Fully Paid'] = 1

    test_data['loan_status'][test_data['loan_status'] == 'Charged Off'] = 0
    test_data['loan_status'][(test_data['Term'] >
                              (test_data['Years'] * 12)) &
                             (test_data['loan_status'] == 'Fully Paid')] = 2
    test_data['loan_status'][test_data['loan_status'] == 'Fully Paid'] = 1

    exog = train_data[['dti',
                       'home_ownership',
                       'inq_last_6mths',
                       'loan_amnt']]

    endog = train_data['loan_status'].astype(float)

    # RandomForestClassifier
    rf = RandomForestClassifier()  # Just use default values
    rf1 = RandomForestClassifier()

    # logistic regression
    logr = LogisticRegression()
    logr1 = LogisticRegression()

    # Now fit the values
    rf = rf.fit(exog, endog)
    rf1 = rf1.fit(exog1, endog1)
    logr = logr.fit(exog, endog)
    logr1 = logr1.fit(exog1, endog1)

    # Now do some cross validation
    scores_r = CV.cross_val_score(rf, exog, endog,
                                  scoring='accuracy', cv=10)
    scores_r1 = CV.cross_val_score(rf1, exog1, endog1,
                                   scoring='accuracy', cv=10)
    scores_l = CV.cross_val_score(logr, exog, endog,
                                  scoring='accuracy', cv=10)
    scores_l1 = CV.cross_val_score(logr1, exog1, endog1,
                                   scoring='accuracy', cv=10)

    # Table that show accuracy score of 2-label and 3-label by using
    # random forest and logistic regression
    a = ['Random forest', 'Logistic regression']
    b = [scores_r1.mean(), scores_l1.mean()]
    c = [scores_r.mean(), scores_l.mean()]
    table1 = Table([a, b, c], names=('Classification method',
                                     '2-label', '3-label'))

    print('Accuarcy score of 2-label and 3-label by using RF and Logit \n')
    print(table1)
    # From the table, one can observe that the logistic regression shows
    # a greater accuracy, if we use the whole prosper data 95%
    # confidence interval print '95% confidence interval for
    # accuracy_score:' print '[', (scores.mean() - 2*(scores.std() /
    # np.sqrt(10))), ',' print scores.mean() + 2*(scores.std() /
    # np.sqrt(10)), ']', '\n'

    prepayment, full, default = [], [], []
    # Now get the mean return and risk for each class
    for rr, c in zip(train_data['Return_rate'], rf.predict(exog)):
        if c == 2:
            prepayment += [rr]
        elif c == 1:
            full += [rr]
        else:
            default += [rr]

    prepayment = pd.DataFrame(data=prepayment, columns=['Prepayment'])
    full = pd.DataFrame(data=full, columns=['Full'])
    default = pd.DataFrame(data=default, columns=['Default'])

    # print 'ARR Prepay:', prepayment.mean()[0], prepayment.std()[0], '\n'
    # print 'ARR Full:', full.mean()[0], full.std()[0], '\n'
    # print 'ARR Default:', default.mean()[0], default.std()[0], '\n'

    # Now do for the test data
    test_exog = test_data[['dti',
                           'home_ownership',
                           'inq_last_6mths',
                           'loan_amnt']]
    test_exog1 = test_data1[['dti',
                             'home_ownership',
                             'inq_last_6mths',
                             'loan_amnt']]
    test_predicted_values = rf.predict(test_exog)
    test_predicted_values1 = rf1.predict(test_exog1)
    test_predicted_values_l = logr.predict(test_exog)
    test_predicted_values1_l = logr1.predict(test_exog1)

    actual_values = test_data['loan_status'].astype(float)
    actual_values1 = test_data1['loan_status'].astype(float)

    a = ['Random forest', 'Logistic regression']
    b = [accuracy_score(test_predicted_values1, actual_values1),
         accuracy_score(test_predicted_values1_l, actual_values1)]
    c = [accuracy_score(test_predicted_values, actual_values),
         accuracy_score(test_predicted_values_l, actual_values)]
    table2 = Table([a, b, c], names=('Classification method',
                                     '2-label', '3-label'))

    print('\n Test accuarcy of 2-label and 3-label by using RF and Logit\n')
    print(table2)
    # From table2, one can observed that logistic regression may be the
    # better choice for 2-label, anad random forest be the better choice
    # for 3-label print 'Test prediction accuracy:', accuracy_score(
    # test_predicted_values, actual_values)

    test_rr = pd.DataFrame(np.zeros((len(actual_values), 2)),
                           columns=['RR', 'Var'])
    test_rl = pd.DataFrame(np.zeros((len(actual_values), 2)),
                           columns=['RR', 'Var'])

    for i, t in zip(range(len(actual_values)), test_predicted_values):
        if t == 2:
            test_rr['RR'][i] = prepayment.mean()[0]
            test_rr['Var'][i] = np.square(prepayment.std()[0])
        elif t == 1:
            test_rr['RR'][i] = full.mean()[0]
            test_rr['Var'][i] = np.square(full.std()[0])
        else:
            test_rr['RR'][i] = default.mean()[0]
            test_rr['Var'][i] = np.square(default.std()[0])

    for i, t in zip(range(len(actual_values)), test_predicted_values_l):
        if t == 2:
            test_rl['RR'][i] = prepayment.mean()[0]
            test_rl['Var'][i] = np.square(prepayment.std()[0])
        elif t == 1:
            test_rl['RR'][i] = full.mean()[0]
            test_rl['Var'][i] = np.square(full.std()[0])
        else:
            test_rl['RR'][i] = default.mean()[0]
            test_rl['Var'][i] = np.square(default.std()[0])
    # print 'RF mse:', mean_squared_error(
    # test_data['Return_rate'], test_rr['RR'])

    # print test_rr.describe()

    # Let us now do kernel method
    kernel_rr = kernel_method(exog, test_exog, rf,
                              train_data['Return_rate'].values)
    kernel_rl = kernel_method(exog, test_exog, logr,
                              train_data['Return_rate'].values)
    kernel_rr1 = kernel_method(exog1, test_exog1, rf1,
                               train_data1['Return_rate'].values)
    kernel_rl1 = kernel_method(exog1, test_exog1, logr1,
                               train_data1['Return_rate'].values)

    # generate table that compare those method
    a = ['Random forest', 'Logistic regression']
    b = [mean_squared_error(test_data1['Return_rate'],
                            kernel_rr1['RR']),
         mean_squared_error(test_data1['Return_rate'],
                            kernel_rl1['RR'])]
    c = [mean_squared_error(test_data['Return_rate'],
                            test_rr['RR']),
         mean_squared_error(test_data['Return_rate'], test_rl['RR'])]
    d = [mean_squared_error(test_data['Return_rate'], kernel_rr['RR']),
         mean_squared_error(test_data['Return_rate'], kernel_rl['RR'])]

    table3 = Table([a, b, c, d],
                   names=('Classification method', '2-label with kernel',
                          '3-label RF', '3-label with kernel'))

    print('\n MSE of 2-label with kernel, '
          '3-label and 3-label '
          'with kerenl by using '
          'random forest and logistic regression\n')
    print(table3)
    # print 'kernel mse:', mean_squared_error(test_data['Return_rate'],
    # kernel_rr['RR'])

    # print kernel_rr.describe()

    # The markowitz portfolio optimization for those mehtod
    f, ax = plt.subplots(2, 3)
    print('\n RF 3-label')
    markowitz(test_rr, 'RF 3-label', ax[0][0])
    print('\n LG 3-label')
    markowitz(test_rl, 'LG 3-label', ax[0][1])
    print('\n kernel 3-label with RF')
    markowitz(kernel_rr, 'RF 3-label kernel', ax[0][2])
    print('\n kernel 3-label with LG')
    markowitz(kernel_rl, 'LG 3-label kernel', ax[1][0])
    print('\n kernel 2-label with RF')
    markowitz(kernel_rr1, 'RF 2-label kernel', ax[1][1])
    print('\n kernel 2-label with LG')
    markowitz(kernel_rl1, 'LG 2-label kernel', ax[1][2])
    plt.show()

    # Seems not good, since the predicted return rates are very close.
    # This is due to that the classification method will return robust
    # predicted values. For example, the logistic regression method
    # predict all the possibality of fully paid rate is 1, I want to see
    # some more accurate number (like 0.98 , 0.56).
if __name__ == '__main__':
    main()
